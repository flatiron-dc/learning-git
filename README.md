# learning-git

# Basic Commands
 - git status: shows the status of all files

 - git add -A : stages the file for commits

 - git commit -m 'Some message'
 - git push origin master
 - git branch : Displays the branches and shows which one you are currently on
 - git checkout -b branch_name : checks out a branch
 - if you end up in a text editor do Esc shift + : and then :x + Enter
 
